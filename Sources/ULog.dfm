object LogForm: TLogForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1051#1086#1075' '#1089#1086#1073#1099#1090#1080#1081
  ClientHeight = 533
  ClientWidth = 973
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    973
    533)
  PixelsPerInch = 120
  TextHeight = 16
  object mLog: TMemo
    Left = 8
    Top = 8
    Width = 961
    Height = 489
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'mLog')
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object bClear: TButton
    Left = 8
    Top = 503
    Width = 241
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 1
    OnClick = bClearClick
  end
  object bSave: TButton
    Left = 248
    Top = 503
    Width = 721
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 2
    OnClick = bSaveClick
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.txt'
    Filter = #1058#1077#1082#1089#1090#1086#1074#1099#1077' '#1092#1072#1081#1083#1099' (*.txt)|*.txt|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Left = 936
    Top = 8
  end
end
