object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = #1050#1083#1077#1090#1086#1095#1085#1099#1081' '#1072#1074#1090#1086#1084#1072#1090
  ClientHeight = 648
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 477
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    784
    648)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox4: TGroupBox
    Left = 520
    Top = 550
    Width = 256
    Height = 58
    Anchors = [akTop, akRight]
    Caption = #1042#1077#1088#1080#1092#1080#1082#1072#1094#1080#1103
    TabOrder = 5
    object Lb_Velocity: TLabel
      Left = 16
      Top = 38
      Width = 80
      Height = 13
      Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1088#1086#1089#1090#1072
    end
    object Result: TButton
      Left = 22
      Top = 13
      Width = 223
      Height = 19
      Caption = #1087#1072#1088#1072#1084#1077#1090#1088#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1082#1086#1085#1094#1077#1085#1090#1088#1072#1094#1080#1080' '#1074' '#1079#1077#1084#1083#1077
      TabOrder = 0
      OnClick = ResultClick
    end
    object ed_velocity: TEdit
      Left = 126
      Top = 33
      Width = 119
      Height = 21
      TabOrder = 1
      Text = '1'
    end
  end
  object GroupBox2: TGroupBox
    Left = 520
    Top = 232
    Width = 256
    Height = 312
    Anchors = [akTop, akRight]
    Caption = ' '#1052#1086#1076#1077#1083#1080#1088#1086#1074#1072#1085#1080#1077' '
    TabOrder = 3
    object Label4: TLabel
      Left = 10
      Top = 16
      Width = 98
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1096#1072#1075#1086#1074':'
    end
    object Label5: TLabel
      Left = 10
      Top = 64
      Width = 77
      Height = 13
      Caption = #1052#1080#1085'. '#1088#1077#1089#1091#1088#1089#1086#1074':'
    end
    object Label6: TLabel
      Left = 141
      Top = 64
      Width = 82
      Height = 13
      Caption = #1052#1072#1082#1089'. '#1088#1077#1089#1091#1088#1089#1086#1074':'
    end
    object Label7: TLabel
      Left = 10
      Top = 112
      Width = 96
      Height = 13
      Caption = #1056#1072#1089#1093'. '#1088#1077#1089'. '#1085#1072' '#1088#1086#1089#1090':'
    end
    object Label8: TLabel
      Left = 141
      Top = 112
      Width = 104
      Height = 13
      Caption = #1056#1072#1089#1093'. '#1088#1077#1089'. '#1085#1072' '#1078#1080#1079#1085#1100':'
    end
    object Label9: TLabel
      Left = 10
      Top = 193
      Width = 214
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1096#1072#1075#1086#1074' '#1076#1086' '#1076#1086#1095#1077#1088#1085#1077#1081' '#1082#1083#1077#1090#1082#1080': 4'
    end
    object Label12: TLabel
      Left = 10
      Top = 219
      Width = 86
      Height = 13
      Caption = #1042#1086#1079#1088#1072#1089#1090' '#1089#1090#1072#1079#1080#1089#1072':'
    end
    object StepCountEdit: TEdit
      Left = 10
      Top = 32
      Width = 105
      Height = 21
      Ctl3D = True
      MaxLength = 4
      NumbersOnly = True
      ParentCtl3D = False
      TabOrder = 0
      Text = '1'
    end
    object StartButton: TButton
      Left = 130
      Top = 28
      Width = 115
      Height = 25
      Caption = #1053#1072#1095#1072#1090#1100
      Enabled = False
      TabOrder = 1
      OnClick = StartButtonClick
    end
    object RemoveAllButton: TButton
      Left = 126
      Top = 274
      Width = 119
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1075#1088#1080#1073#1099
      Enabled = False
      TabOrder = 7
      OnClick = RemoveAllButtonClick
    end
    object JmpMemCheckBox: TCheckBox
      Left = 10
      Top = 158
      Width = 131
      Height = 17
      Caption = #1055#1072#1084#1103#1090#1100' '#1087#1088#1080' '#1087#1088#1099#1078#1082#1077
      Checked = True
      State = cbChecked
      TabOrder = 6
      OnClick = JmpMemCheckBoxClick
    end
    object ResMinEdit: TEdit
      Left = 10
      Top = 80
      Width = 105
      Height = 21
      TabOrder = 2
      Text = '0.01'
    end
    object ResMaxEdit: TEdit
      Left = 141
      Top = 80
      Width = 104
      Height = 21
      TabOrder = 3
      Text = '0.03'
    end
    object GrowthConsumptionEdit: TEdit
      Left = 10
      Top = 128
      Width = 105
      Height = 21
      TabOrder = 4
      Text = '0.02'
    end
    object LivingConsumptionEdit: TEdit
      Left = 141
      Top = 128
      Width = 104
      Height = 21
      TabOrder = 5
      Text = '0.01'
    end
    object NoTorCheckBox: TCheckBox
      Left = 10
      Top = 174
      Width = 131
      Height = 17
      Caption = #1054#1090#1082#1083#1102#1095#1080#1090#1100' '#1058#1054#1056
      TabOrder = 8
    end
    object UpDown1: TUpDown
      Left = 228
      Top = 186
      Width = 17
      Height = 25
      Min = 1
      Position = 4
      TabOrder = 9
      OnMouseUp = UpDown1MouseUp
    end
    object ScaleBar: TTrackBar
      Left = 3
      Top = 244
      Width = 250
      Height = 28
      Max = 9
      Min = 1
      Position = 5
      TabOrder = 10
      OnChange = ScaleBarChange
    end
    object bShowLog: TButton
      Left = 10
      Top = 274
      Width = 97
      Height = 25
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1083#1086#1075
      TabOrder = 11
      OnClick = bShowLogClick
    end
    object updownStasisAge: TUpDown
      Left = 222
      Top = 216
      Width = 17
      Height = 21
      Associate = StasisAgeEdit
      Min = 1
      Max = 10000
      Position = 2
      TabOrder = 12
      OnMouseUp = UpDown1MouseUp
    end
    object StasisAgeEdit: TEdit
      Left = 103
      Top = 216
      Width = 119
      Height = 21
      TabOrder = 13
      Text = '2'
    end
    object cbShowMashrooms: TCheckBox
      Left = 131
      Top = 174
      Width = 119
      Height = 17
      Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1075#1088#1080#1073#1099
      Checked = True
      State = cbChecked
      TabOrder = 14
      OnClick = cbShowMashroomsClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 520
    Top = 158
    Width = 256
    Height = 74
    Anchors = [akTop, akRight]
    Caption = ' '#1047#1072#1075#1088#1091#1079#1082#1072' '#1088#1077#1089#1091#1088#1089#1086#1074' '
    TabOrder = 2
    DesignSize = (
      256
      74)
    object OpenResButton: TButton
      Left = 3
      Top = 39
      Width = 137
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
      Enabled = False
      TabOrder = 1
      OnClick = OpenResButtonClick
    end
    object NormalizeCheckBox: TCheckBox
      Left = 10
      Top = 16
      Width = 153
      Height = 17
      Caption = #1053#1086#1088#1084#1072#1083#1080#1079#1086#1074#1072#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object bLoadTestData: TButton
      Left = 146
      Top = 39
      Width = 107
      Height = 25
      Caption = #1044#1083#1103' '#1090#1077#1089#1090#1072
      TabOrder = 2
      OnClick = bLoadTestDataClick
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 628
    Width = 784
    Height = 20
    Panels = <
      item
        Text = #1057#1086#1079#1076#1072#1081#1090#1077' '#1087#1086#1083#1077' '#1076#1083#1103' '#1085#1072#1095#1072#1083#1072' '#1088#1072#1073#1086#1090#1099
        Width = 200
      end
      item
        Text = #1056#1072#1079#1084#1077#1088' '#1087#1086#1083#1103': '#1085#1077' '#1079#1072#1076#1072#1085
        Width = 140
      end
      item
        Width = 50
      end>
  end
  object FieldGrid: TStringGrid
    Left = 8
    Top = 5
    Width = 506
    Height = 619
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultColWidth = 10
    DefaultRowHeight = 10
    DefaultDrawing = False
    FixedCols = 0
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goThumbTracking]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Visible = False
    OnDrawCell = FieldGridDrawCell
    OnMouseMove = FieldGridMouseMove
    ColWidths = (
      10
      10
      10
      10
      10)
    RowHeights = (
      10
      10
      10
      10
      10)
  end
  object GroupBox1: TGroupBox
    Left = 520
    Top = 8
    Width = 256
    Height = 148
    Anchors = [akTop, akRight]
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
    TabOrder = 1
    object Label1: TLabel
      Left = 10
      Top = 16
      Width = 95
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088#1085#1086#1089#1090#1100' '#1087#1086#1083#1103':'
    end
    object FieldSizeEdit: TEdit
      Left = 10
      Top = 32
      Width = 105
      Height = 21
      MaxLength = 3
      NumbersOnly = True
      TabOrder = 0
      Text = '11'
    end
    object CreateFieldButton: TButton
      Left = 130
      Top = 28
      Width = 115
      Height = 25
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1087#1086#1083#1077
      TabOrder = 1
      OnClick = CreateFieldButtonClick
    end
    object pcMushrooms: TPageControl
      Left = 2
      Top = 58
      Width = 252
      Height = 88
      ActivePage = tsCurrentMushroom
      Align = alBottom
      TabOrder = 2
      object tsCurrentMushroom: TTabSheet
        Caption = #1047#1072#1089#1077#1103#1090#1100' '#1075#1088#1080#1073
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 244
          Height = 60
          Align = alClient
          ParentBackground = False
          TabOrder = 4
          object Label10: TLabel
            Left = 8
            Top = 31
            Width = 17
            Height = 13
            Caption = 'X ='
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TLabel
            Left = 89
            Top = 31
            Width = 17
            Height = 13
            Caption = 'Y ='
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 10
            Top = 3
            Width = 105
            Height = 13
            Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099' '#1079#1072#1089#1077#1074#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
        end
        object SeedXEdit: TEdit
          Left = 28
          Top = 28
          Width = 47
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 0
          Text = '6'
        end
        object SeedYEdit: TEdit
          Left = 108
          Top = 28
          Width = 47
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 1
          Text = '6'
        end
        object SeedXYButton: TButton
          Left = 161
          Top = 25
          Width = 79
          Height = 25
          Caption = #1047#1072#1089#1077#1103#1090#1100
          Enabled = False
          TabOrder = 2
          OnClick = SeedXYButtonClick
        end
        object bRandomXY: TButton
          Left = 121
          Top = 3
          Width = 118
          Height = 15
          Caption = #1057#1083#1091#1095#1072#1081#1085#1086
          TabOrder = 3
          OnClick = bRandomXYClick
        end
      end
      object tsRandomMushrooms: TTabSheet
        Caption = #1047#1072#1089#1077#1103#1090#1100' '#1085#1077#1089#1082#1086#1083#1100#1082#1086' '#1075#1088#1080#1073#1086#1074
        ImageIndex = 1
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 244
          Height = 60
          Align = alClient
          ParentBackground = False
          TabOrder = 2
          object Label2: TLabel
            Left = 10
            Top = 7
            Width = 72
            Height = 13
            Caption = #1063#1080#1089#1083#1086' '#1075#1088#1080#1073#1086#1074':'
          end
        end
        object SeedCountEdit: TEdit
          Left = 10
          Top = 28
          Width = 103
          Height = 21
          MaxLength = 3
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object SeedButton: TButton
          Left = 119
          Top = 25
          Width = 120
          Height = 25
          Caption = #1047#1072#1089#1077#1103#1090#1100
          Enabled = False
          TabOrder = 1
          OnClick = SeedButtonClick
        end
      end
    end
  end
  object OpenResDialog: TOpenDialog
    Filter = 'Quasy-natural file (*.qnl)|*.qnl|Unnormized file (*.csv)|*.csv'
    Left = 456
    Top = 16
  end
end
