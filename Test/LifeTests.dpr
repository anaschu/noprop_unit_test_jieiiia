program LifeTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  TestUModel in 'TestUModel.pas',
  UModel in '..\Sources\UModel.pas',
  UField in '..\Sources\UField.pas',
  UCoord in '..\Sources\UCoord.pas',
  UCell in '..\Sources\UCell.pas',
  ULimits in '..\Sources\ULimits.pas',
  ULog in '..\Sources\ULog.pas' {LogForm},
  FMain in '..\Sources\FMain.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.

